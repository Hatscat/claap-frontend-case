# Claap Frontend Case: [invite teammates](https://hatscat.gitlab.io/claap-frontend-case)

> [Frontend Case Instructions](https://www.notion.so/Frontend-Case-aadae875d0a2490eb1915772858386fa)

## Setup instructions

requirement: `NodeJS`

```sh
npm install
```

## Commands

* to run directly from sources

```sh
npm run dev
```

* to build then run

```sh
npm run build

npm run start
```

* to execute tests

```sh
# only unit tests
npm t

# tests coverage
npm run test:cov

# integration test
# be sure to have the local server running with `npm run dev`
npm run cy:open

# check linter and types
npm run scan

# shortcut to check linter, types and tests coverage
npm run validate
```
