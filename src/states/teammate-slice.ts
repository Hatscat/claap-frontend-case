import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { isMember, Member, PendingMember, Teammate } from '../entities/teammate'
import type { RootState } from '../store'

export type TeammateState = {
  memberIndex: { [id: Member['id']]: Member }
  pendingMemberIndex: { [email: PendingMember['email']]: PendingMember }
}

const initialState: TeammateState = {
  memberIndex: {},
  pendingMemberIndex: {},
}

export const teammateSlice = createSlice({
  name: 'teammate',
  initialState,
  reducers: {
    addTeammates(state, action: PayloadAction<Array<Teammate>>) {
      action.payload.forEach((teammate) => {
        if (isMember(teammate)) {
          state.memberIndex[teammate.id] = teammate
        } else {
          state.pendingMemberIndex[teammate.email] = teammate
        }
      })
    },
    reset: {
      reducer: () => initialState,
      prepare: () => ({ payload: {} }),
    },
  },
})

export const { addTeammates, reset } = teammateSlice.actions

export const selectMembers = (state: RootState): Member[] =>
  Object.values(state.teammate.memberIndex)

export const selectPendingMembers = (state: RootState): PendingMember[] =>
  Object.values(state.teammate.pendingMemberIndex)
