import { Member, PendingMember, Teammate } from '../entities/teammate'
import { store } from '../store'
import {
  addTeammates,
  reset,
  selectMembers,
  selectPendingMembers,
} from './teammate-slice'

describe('teammateSlice', () => {
  const teammateSlice = () => store.getState().teammate

  beforeEach(() => {
    store.dispatch(reset())
  })

  test('addTeammates action', () => {
    // Given
    const members: Member[] = ['Bob', 'Tara', 'Bill'].map((name, i) => ({
      firstName: name,
      lastName: name + name,
      email: `${name}@claap.io`,
      id: `id${i}`,
    }))
    const pendingMembers = ['Foo', 'Bar'].map((name) => ({
      email: `${name}@domain.com`,
    })) as PendingMember[]

    const teammates: Teammate[] = [...members, ...pendingMembers]

    // When
    store.dispatch(addTeammates(teammates))

    // Then
    expect(teammateSlice()).toEqual({
      memberIndex: {
        id0: members[0],
        id1: members[1],
        id2: members[2],
      },
      pendingMemberIndex: {
        'Foo@domain.com': pendingMembers[0],
        'Bar@domain.com': pendingMembers[1],
      },
    })
  })

  test('selectMembers selector', () => {
    // Given
    const members: Member[] = ['Bob', 'Tara', 'Bill'].map((name, i) => ({
      firstName: name,
      lastName: name + name,
      email: `${name}@claap.io`,
      id: `id${i}`,
    }))
    store.dispatch(addTeammates(members))

    // When
    const result = selectMembers(store.getState())

    // Then
    expect(result).toEqual(members)
  })

  test('selectPendingMembers selector', () => {
    // Given
    const pendingMembers = ['Foo', 'Bar'].map((name) => ({
      email: `${name}@domain.com`,
    })) as PendingMember[]

    store.dispatch(addTeammates(pendingMembers))

    // When
    const result = selectPendingMembers(store.getState())

    // Then
    expect(result).toEqual(pendingMembers)
  })
})
