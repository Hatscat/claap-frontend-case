import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import translations from './translations.json'

export function translate(): void {
  i18n.use(initReactI18next).init({
    resources: translations,
    lng: 'en',
  })
}
