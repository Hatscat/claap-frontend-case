import { extendTheme } from '@chakra-ui/react'
import { Button } from './Button'
import { Modal } from './Modal'

export const theme = extendTheme({
  components: {
    Button,
    Modal,
  },
  colors: {
    main: '#2C54EA',
    second: '#EE748F',
    text: {
      white: '#FFFFFF',
      light: '#DBE1E6',
      grey: '#8C9DB5',
    },
    background: {
      white: '#FFFFFF',
      light: '#E9EEFF',
      main: '#272D45',
      dark: '#202437',
      black: '#000000',
    },
    border: {
      focused: '#383c56',
      light: '#4F5579',
      base: '#272D45',
    },
  },
  fonts: {
    body: 'Lato, sans-serif',
    heading: 'Lato, serif',
  },
  fontSizes: {
    xs: '11px',
    sm: '13px',
    md: '15px',
    lg: '24px',
    xl: '30px',
  },
  space: {
    0.5: '1px',
    1: '2px',
    1.5: '3px',
    2: '4px',
    2.5: '6px',
    3: '8px',
    3.5: '10px',
    4: '12px',
    5: '16px',
    6: '24px',
    7: '32px',
    8: '48px',
    9: '64px',
  },
  radii: {
    sm: '6px',
    base: '10px',
  },
})
