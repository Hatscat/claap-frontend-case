export const Modal = {
  parts: ['dialog', 'header', 'body', 'footer'],

  // Styles for the base style
  baseStyle: {
    dialog: {
      borderRadius: 'base',
      background: 'background.main',
      color: 'text.light',
      p: 9,
    },
    header: {
      fontSize: 'lg',
      fontWeight: 'md',
      textAlign: 'center',
      p: 0,
      mb: 7,
    },
    body: {
      p: 0,
      mb: 6,
    },
    footer: {
      p: 0,
    },
  },
  // Styles for the size variations
  sizes: {
    md: {
      dialog: {
        width: 'min',
        maxWidth: 'container.md',
      },
    },
  },
  // Styles for the visual style variations
  variants: {},
  // The default `size` or `variant` values
  defaultProps: {
    size: 'md',
  },
}
