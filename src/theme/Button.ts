export const Button = {
  // Styles for the base style
  baseStyle: {
    borderRadius: 'base',
    fontSizes: 'sm',
  },
  // Styles for the size variations
  sizes: {
    md: {
      px: 6,
      py: 3.5,
    },
  },
  // Styles for the visual style variations
  variants: {
    primary: {
      backgroundColor: 'main',
      color: 'text.white',
      fontWeight: 'bold',
    },
  },
  // The default `size` or `variant` values
  defaultProps: {
    size: 'md',
    variant: 'primary',
  },
}
