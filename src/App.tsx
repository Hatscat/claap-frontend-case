import React from 'react'
import { LandingPage } from './pages/Landing'

export function App(): JSX.Element {
  return (
    <>
      <LandingPage />
    </>
  )
}
