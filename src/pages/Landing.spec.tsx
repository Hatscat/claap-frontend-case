import { useDisclosure } from '@chakra-ui/react'
import { render } from '@testing-library/react'
import { when } from 'jest-when'
import React from 'react'
import { useSelector } from 'react-redux'
import { Member, PendingMember } from '../entities/teammate'
import { selectMembers, selectPendingMembers } from '../states/teammate-slice'
import { LandingPage } from './Landing'

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}))
jest.mock('@chakra-ui/react', () => ({
  ...jest.requireActual('@chakra-ui/react'),
  useDisclosure: jest.fn(),
}))

describe('Landing page', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  test('render the page with teammates table', () => {
    // Given
    const members: Member[] = ['Bob', 'Tara', 'Bill'].map((name, i) => ({
      firstName: name,
      lastName: name + name,
      email: `${name}@claap.io`,
      id: `id${i}`,
    }))
    const pendingMembers = ['Foo', 'Bar'].map((name) => ({
      email: `${name}@domain.com`,
    })) as PendingMember[]

    when(useDisclosure).mockReturnValue({
      isOpen: false,
      onOpen: jest.fn(),
      onClose: jest.fn(),
    } as any)

    when(useSelector as jest.Mock)
      .calledWith(selectMembers)
      .mockReturnValue(members)
    when(useSelector as jest.Mock)
      .calledWith(selectPendingMembers)
      .mockReturnValue(pendingMembers)

    // When
    const { container, getByText } = render(<LandingPage />)

    // Then
    expect(getByText('Members')).toBeInTheDocument()
    expect(getByText('Pending Members')).toBeInTheDocument()
    expect(getByText('Invite teammates')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
})
