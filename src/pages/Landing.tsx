import { Button, Flex, Spacer, useDisclosure } from '@chakra-ui/react'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Popin } from '../components/Popin'
import { TeammatesTable } from '../components/TeammatesTable'

export function LandingPage(): JSX.Element {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { t } = useTranslation()

  return (
    <Flex
      align="center"
      background="background.white"
      direction="column"
      height="100vh"
      justify="space-between"
      p={4}
    >
      <TeammatesTable />

      <Button autoFocus onClick={onOpen}>
        {t('page.landing.button.invite-members')}
      </Button>
      <Spacer />

      <Popin isOpen={isOpen} onClose={onClose} />
    </Flex>
  )
}
