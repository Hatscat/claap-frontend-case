import { ChakraProvider } from '@chakra-ui/react'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { App } from './App'
import { translate } from './i18n'
import { store } from './store'
import { theme } from './theme/theme'

function init() {
  translate()

  render(
    <Provider store={store}>
      <ChakraProvider theme={theme}>
        <App />
      </ChakraProvider>
    </Provider>,
    document.querySelector('#root')
  )
}

init()
