export type Teammate = Member | PendingMember

export type Member = {
  firstName: string
  lastName: string
  email: string
  id: string
}

export type PendingMember = {
  email: Email
}

export function isMember(value: Member | PendingMember): value is Member {
  return 'id' in value
}

export function isEmail(value: string): value is Email {
  // Regex from https://regex101.com/library/jZ2cC4
  const validEmailRegex =
    /^[\da-z]+(?:[._-][\da-z]+)*@[\da-z]+(?:[.-][\da-z]+)*\.[a-z]{2,}$/i
  return validEmailRegex.test(value)
}

declare const validEmail: unique symbol

type Email = string & {
  [validEmail]: true
}
