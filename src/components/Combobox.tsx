import { Box, Flex, Spinner } from '@chakra-ui/react'
import React, { ChangeEvent, KeyboardEvent, useRef, useState } from 'react'
import { SelectOption } from './SelectOption'

export type OptionIndex<Value> = {
  [label: string]: Value
}

export type Props<Value> = {
  placeholder: string
  loadOptions: (input: string) => Promise<OptionIndex<Value>>
  onChange: (selectedOptions: OptionIndex<Value>) => void
}

export function Combobox<Value>({
  loadOptions,
  placeholder,
  onChange,
}: Props<Value>): JSX.Element {
  const input = useRef(null as HTMLInputElement | null)
  const [focused, setFocused] = useState(false)
  const [loading, setLoading] = useState(false)
  const [selectedOptions, setSelectedOptions] = useState(
    {} as OptionIndex<Value>
  )
  const [availableOptions, setAvailableOptions] = useState(
    {} as OptionIndex<Value>
  )
  const [focusedOptionIndex, setFocusedOptionIndex] = useState(0)
  const labels = Object.keys(selectedOptions)

  const handleOptionSelect = (label: string) => {
    setFocusedOptionIndex(0)
    setAvailableOptions({})
    if (input.current) {
      input.current.value = ''
      input.current.focus()
    }

    const newSelectedOptions = {
      ...selectedOptions,
      [label]: availableOptions[label],
    }
    setSelectedOptions(newSelectedOptions)
    onChange(newSelectedOptions)
  }

  const handleInput = async (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target
    setLoading(true)

    const newOptions = await loadOptions(value)
    Object.keys(newOptions).forEach((label) => {
      if (selectedOptions[label]) {
        delete newOptions[label]
      }
    })
    setAvailableOptions(newOptions)
    setFocusedOptionIndex(
      focusedOptionIndex % Math.max(1, Object.keys(newOptions).length)
    )
    setLoading(false)
  }

  const handleKeyDown = ({ key }: KeyboardEvent) => {
    const optionLabels = Object.keys(availableOptions)
    if (key === 'Enter' && optionLabels.length > 0) {
      handleOptionSelect(optionLabels[focusedOptionIndex])
      return
    }
    if (key === 'Backspace' && input.current?.value === '') {
      const lastestLabel = labels[labels.length - 1]
      const newSelectedOptions = {
        ...selectedOptions,
      }
      delete newSelectedOptions[lastestLabel]
      setSelectedOptions(newSelectedOptions)
      onChange(newSelectedOptions)
      return
    }
    const direction = { ArrowDown: 1, ArrowUp: -1 }[key] ?? 0
    if (direction !== 0) {
      const { length } = optionLabels
      setFocusedOptionIndex((length + focusedOptionIndex + direction) % length)
    }
  }

  const handleRemoveLabel = (label: string) => {
    const newSelectedOptions = {
      ...selectedOptions,
    }
    delete newSelectedOptions[label]
    setSelectedOptions(newSelectedOptions)
    onChange(newSelectedOptions)
  }

  return (
    <Flex direction="column">
      <Flex
        _hover={{ borderColor: 'border.light' }}
        align="center"
        background="background.dark"
        border="1px"
        borderColor={focused ? 'border.focused' : 'border.base'}
        borderRadius="base"
        cursor="text"
        onClick={() => input.current?.focus()}
        p={1}
        position="relative"
        transition="all ease 0.2s"
        width="306px"
        wrap="wrap"
      >
        {labels.map((label) => (
          <SelectOption key={label} label={label} onClose={handleRemoveLabel} />
        ))}

        <Box
          ref={input}
          _focus={{ outline: 'none' }}
          _placeholder={{
            fontFamily: 'Lato',
            fontSize: 'sm',
            color: 'text.white',
            opacity: 0.3,
          }}
          as="input"
          autoComplete="email"
          autoFocus
          background="background.dark"
          color="text.light"
          flexGrow={1}
          fontSize="sm"
          minHeight="36px"
          ml={labels.length === 0 ? 5 : 0}
          onBlur={() => setFocused(false)}
          onFocus={() => setFocused(true)}
          onInput={handleInput}
          onKeyDown={handleKeyDown}
          placeholder={labels.length === 0 ? placeholder : undefined}
          size={(input.current?.value.length ?? 0) + 3}
        />

        {loading && (
          <Spinner
            position="absolute"
            right="8px"
            size="xs"
            top="calc(50% - 5px)"
          />
        )}
      </Flex>

      <Box position="relative">
        <Flex
          borderRadius="sm"
          direction="column"
          overflow="hidden"
          position="absolute"
          top={0}
          width="100%"
        >
          {Object.keys(availableOptions).map((label, i) => {
            const isFocused = focusedOptionIndex === i
            return (
              <Flex
                key={label}
                background={isFocused ? 'background.black' : 'background.dark'}
                cursor="pointer"
                justify="center"
                onClick={() => handleOptionSelect(label)}
              >
                <SelectOption label={label} />
              </Flex>
            )
          })}
        </Flex>
      </Box>
    </Flex>
  )
}
