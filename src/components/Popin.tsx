import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  ModalProps,
  Stack,
  Text,
} from '@chakra-ui/react'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { TeammatesForm } from './TeammatesForm'

type Props = Pick<ModalProps, 'isOpen' | 'onClose'>

export function Popin({ isOpen, onClose }: Props): JSX.Element {
  const { t } = useTranslation()

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />

      <ModalContent>
        <ModalHeader>{t('component.popin.title')}</ModalHeader>
        <ModalCloseButton />

        <ModalBody>
          <Stack spacing={5}>
            <Text fontSize="md">{t('component.popin.subtitle')}</Text>
            <Text color="text.grey" fontSize="md">
              {t('component.popin.description')}
            </Text>
          </Stack>
        </ModalBody>

        <ModalFooter>
          <TeammatesForm onSubmit={onClose} />
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}
