import {
  Heading,
  Spacer,
  Stack,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react'
import React from 'react'
import { useSelector } from 'react-redux'
import { selectMembers, selectPendingMembers } from '../states/teammate-slice'

export function TeammatesTable(): JSX.Element {
  const members = useSelector(selectMembers)
  const pendingMembers = useSelector(selectPendingMembers)
  const isDataEmpty = members.length === 0 && pendingMembers.length === 0
  if (isDataEmpty) {
    return <Spacer />
  }

  return (
    <Stack pb={7} spacing={5}>
      {members.length > 0 && (
        <>
          <Heading size="lg">Members</Heading>
          <Table variant="striped">
            <Thead>
              <Tr>
                <Th>first name</Th>
                <Th>last name</Th>
                <Th>email</Th>
              </Tr>
            </Thead>
            <Tbody>
              {members.map(({ firstName, lastName, email, id }) => (
                <Tr key={id}>
                  <Td>{firstName}</Td>
                  <Td>{lastName}</Td>
                  <Td>{email}</Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </>
      )}

      {pendingMembers.length > 0 && (
        <>
          <Spacer />
          <Heading size="lg">Pending Members</Heading>
          <Table variant="striped">
            <Thead>
              <Tr>
                <Th>email</Th>
              </Tr>
            </Thead>
            <Tbody>
              {pendingMembers.map(({ email }) => (
                <Tr key={email}>
                  <Td>{email}</Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </>
      )}
    </Stack>
  )
}
