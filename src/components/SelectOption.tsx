import { CloseIcon, EmailIcon } from '@chakra-ui/icons'
import { Avatar, Box, Flex } from '@chakra-ui/react'
import React from 'react'
import { isEmail } from '../entities/teammate'

type Props = {
  label: string
  onClose?: (label: string) => void
}

export function SelectOption({ label, onClose }: Props): JSX.Element {
  return (
    <Flex
      align="center"
      border="1px"
      borderColor="second"
      borderRadius="sm"
      color="second"
      fontSize="xs"
      mr={3}
      my={1}
      py={2}
    >
      {isEmail(label) ? (
        <EmailIcon color="second" h={3} mx={3.5} w={3} />
      ) : (
        <Avatar
          background="second"
          color="text.white"
          mx={3.5}
          name={label}
          size="2xs"
        />
      )}
      {label}
      {onClose ? (
        <CloseIcon
          color="second"
          cursor="pointer"
          h={2}
          mx={3}
          onClick={() => onClose(label)}
          w={3}
        />
      ) : (
        <Box w="20px" />
      )}
    </Flex>
  )
}
