import { Button, Flex, useToast } from '@chakra-ui/react'
import { ResultAsync } from 'neverthrow'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { searchUser } from '../api-client'
import { isEmail, Teammate } from '../entities/teammate'
import { addTeammates } from '../states/teammate-slice'
import { Combobox, OptionIndex } from './Combobox'

type Props = {
  onSubmit: () => void
}

export function TeammatesForm({ onSubmit }: Props): JSX.Element {
  const { t } = useTranslation()
  const toast = useToast()
  const [teammates, setTeammates] = useState([] as Teammate[])
  const dispatch = useDispatch()

  const handleSubmit = () => {
    dispatch(addTeammates(teammates))
    onSubmit()
  }

  const handleChange = (newTeammates: OptionIndex<Teammate>) => {
    setTeammates(Object.values(newTeammates))
  }

  const loadOptions = async (input: string): Promise<OptionIndex<Teammate>> => {
    const usersResult = await ResultAsync.fromPromise(searchUser(input), String)
    if (usersResult.isErr()) {
      if (!toast.isActive('load-error-toast')) {
        toast({
          id: 'load-error-toast',
          title: t('error.backend.fetch-failure.title'),
          description: t('error.backend.fetch-failure.description'),
          status: 'error',
          duration: 6000,
          isClosable: true,
        })
      }
      return {}
    }
    if (usersResult.value.length === 0 && isEmail(input)) {
      return { [input]: { email: input } }
    }
    return usersResult.value.reduce(
      (index, user) => ({ ...index, [user.firstName]: user }),
      {}
    )
  }

  return (
    <Flex align="center">
      <Combobox
        loadOptions={loadOptions}
        onChange={handleChange}
        placeholder={t('component.popin.search.placeholder')}
      />

      <Button
        disabled={teammates.length === 0}
        ml={5}
        onClick={handleSubmit}
        type="submit"
      >
        {t('component.popin.button.submit')}
      </Button>
    </Flex>
  )
}
