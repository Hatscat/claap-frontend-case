import { configureStore } from '@reduxjs/toolkit'
import { teammateSlice } from './states/teammate-slice'

export const store = configureStore({
  reducer: {
    teammate: teammateSlice.reducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
